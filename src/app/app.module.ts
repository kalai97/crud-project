import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsComponent } from './pages/products/products.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditComponent } from './pages/products/edit/edit.component';
import { AddComponent } from './pages/products/add/add.component';
import { ShoppingpageComponent } from './layout/shoppingpage/shoppingpage.component';
import { RegisterpageComponent } from './layout/registerpage/registerpage.component';
import { LoginpageComponent } from './layout/loginpage/loginpage.component';
import { CartpageComponent } from './layout/cartpage/cartpage.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    EditComponent,
    AddComponent,
    ShoppingpageComponent,
    RegisterpageComponent,
    LoginpageComponent,
    CartpageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
