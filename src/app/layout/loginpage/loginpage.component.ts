import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.css']
})
export class LoginpageComponent implements OnInit {
  userLogin!: FormGroup;
  userloggedin:any;
  storedData:[]=[];
  constructor(
    private fb:FormBuilder,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.userLogin = this.fb.group({
      mobileno: ['', [Validators.required,Validators.pattern("[1-9]{1}[0-9]{9}")]],
      password: ['', [Validators.required]]
    })
  }

  Loginuser(){
    console.log(this.userLogin.value);
    var found = false;
    var usermobileno = this.userLogin.value.mobileno;
    var userpassword = this.userLogin.value.password;
    this.storedData=JSON.parse(localStorage.getItem("userdata")|| '{}');
    this.storedData.forEach(function(obj:any){
      console.log("32",obj.usermobileno);
		  if(obj.mobileno==usermobileno){
		  found = true;
		  }
		})
    if(found){
      localStorage.setItem("userloggedin",JSON.stringify(this.userLogin.value))
      localStorage.getItem("userloggedin");
      this.userloggedin = JSON.parse(localStorage.getItem('userloggedin')|| '{}');
      this.router.navigate(['/shoppage']);
    }
    else{
      alert("enter correct mobileno and password");
      this.router.navigate(['/signup']);
    }
  }

}
