import { ThisReceiver } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { resetFakeAsyncZone } from '@angular/core/testing';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-shoppingpage',
  templateUrl: './shoppingpage.component.html',
  styleUrls: ['./shoppingpage.component.css']
})
export class ShoppingpageComponent implements OnInit {

  LoggedIn = false;
  obj:any;
  count = 0;
  totalquanity:any;
  userpurchaselist:any[] = [];
  purchaselist:any
  totalprice:any;
  constructor(
    private modalService:NgbModal,
    private route:Router,
  ) { }

  products =[
    {
        id:1,
        imageIcon:'/assets/oneplus-8pro.jpeg',
        name:'mobile',
        price:'5000',
        quanity:1,
    },
    {
      id:2,
      imageIcon:'/assets/oppo-a31.jpg',
      name:'oppo-a31',
      price:'10000',
      quanity:1,
    },
    {
    id:3,
    imageIcon:'/assets/oppo-a74g.jpg',
    name:'oppo-a74g',
    price:'12000',
    quanity:1,
   },
   {
    id:4,
   imageIcon:'/assets/oppo-f19.jpeg',
   name:'oppo-f19',
   price:'15000',
   quanity:1,
   },
   {
    id:5,
   imageIcon:'/assets/oppoa-53s.jpg',
   name:'oppoa-53s',
   price:'25000',
   quanity:1,
  },
  {
    id:6,
    imageIcon:'/assets/redmi9.jpg',
    name:'redmi9',
    price:'10000',
    quanity:1,
   },
   {
    id:7,
    imageIcon:'/assets/redmi9a.jpg',
    name:'redmi9a',
    price:'14000',
    quanity:1,
   },
   {
    id:8,
    imageIcon:'/assets/redminote10s.webp',
    name:'redminote10s',
    price:'12000',
    quanity:1,
   },
  ]

  ngOnInit(): void {
    
  }
  addcart(data:any){
    if(localStorage.getItem("userloggedin")){
        this.count = this.count + 1;
        console.log("name",data.name);
        const obj ={
      id:data.id,
      image:data.imageIcon,
      name:data.name,
      price:data.price,
      quanity:data.quanity,
       }
      console.log("show id",obj.id);
      if(localStorage.getItem("userproductlist")){
		  this.userpurchaselist = JSON.parse(localStorage.getItem("userproductlist") || '{}');
      }

      this.userpurchaselist.forEach(function(x){
      if(x.id==obj.id){
        obj.quanity = obj.quanity + 1;
      }
      })
 
      this.purchaselist = obj;
	    this.userpurchaselist.push(this.purchaselist);

	    localStorage.setItem("userproductlist",JSON.stringify(this.userpurchaselist));
  }
  else{
    this.route.navigate(['/login']);
  }
    
  }
  purchaselistmodal(content:any){
    this.modalService.open(content,{size:'lg'})
    localStorage.removeItem('userproductlist');
  }
  add(data:any){
    console.log("show selected id",data.id);
    data.quanity = data.quanity + 1;    
  }
  subract(data:any,i:any){
    data.quanity = data.quanity - 1;
    console.log(i);
    if(data.quanity == 0){
      this.userpurchaselist.splice(i,1);
    }
  }

  gettotalprice(data:any){
     var total = 0;
     for (var i = 0; i < this.userpurchaselist.length; i++) {
      var data = this.userpurchaselist[i];
      total += (data.quanity * data.price);
   }
   return total;
  }
  
  pay(){
    // this.count = 0;
    this.modalService.dismissAll();
    // this.userpurchaselist=[];
    // localStorage.removeItem('userloggedin');

    // open payment option
    this.route.navigate(['/paymentpage']);
  }
  close(){
    // this.count = 0;
    // this.userpurchaselist=[];
    this.modalService.dismissAll();
  }
}
