import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registerpage',
  templateUrl: './registerpage.component.html',
  styleUrls: ['./registerpage.component.css']
})
export class RegisterpageComponent implements OnInit {
  userregisterform!: FormGroup;
  data:any;
  newuser:any;
  registerdata:any;
  registeruserdata:any[]= [];
  existinguser:any
  user:any = {};


  constructor(
    private fb:FormBuilder,
	private router:Router,
  ) { }

  ngOnInit(): void {
    this.userregisterform=this.fb.group({
      username:['',[Validators.required]],
      mobileno:['',[Validators.required, Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      password:['',[Validators.required]],
    });
  }

  Signupuser(){
	  var storedData: any[] = [];
		var found = false;
		var userObject = this.userregisterform.value;
		var usermobileno = this.userregisterform.value.mobileno;
		if(localStorage.getItem("userdata")){
		    storedData = JSON.parse(localStorage.getItem("userdata") || '{}');
		    console.log("parse data",storedData);
		}
		storedData.forEach(function(obj){
		  if(obj.mobileno==usermobileno){
		  found = true;
		  }
		  console.log(obj.mobileno)
		})
		if(found){
			alert("number already registered")
		}
		else{
			storedData.push(userObject);
			this.router.navigate(['/shoppage']);
		}
		var userObject = this.userregisterform.value;

		localStorage.setItem("userdata",JSON.stringify(storedData));
		localStorage.getItem("userdata");
		console.log("60",this.data);

	// ===================================================================================
	

      
  }

  //  addUser(_newuser: any){
  //   this.registeruserdata=[];
  //   console.log("41");
  //   if(localStorage.getItem('Registereduser')){
  //      this.registeruserdata = JSON.parse('Registereduser');
  //      this.registeruserdata = [this.newuser, this.registeruserdata];
  //   }
  //   else{
  //     this.registeruserdata = [this.newuser];
  //   }
  //    localStorage.setItem('Registereduser',JSON.stringify(this.registeruserdata))
  // }
  
//   Signupuser(){
// console.log(this.userregisterform.value);
// this.user = Object.assign(this.user,this.userregisterform.value);
// this.addUser(this.user)
// }
// addUser(user:any){
// let users: any[] = [];
// if(localStorage.getItem('Users')){
//   this.registeruserdata = JSON.parse(localStorage.getItem('Users')|| '{}');
//   this.registeruserdata = [user,...users]
// }
// else{
//   users = [user];
// }
// localStorage.setItem('Users',JSON.stringify(user))
// }

// Signupser(){
//   return this.userregisterform.value;
 
// }

//  adduser(){
//    console.log(this.userregisterform.value)
//  }
}



