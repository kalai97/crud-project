import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-cartpage',
  templateUrl: './cartpage.component.html',
  styleUrls: ['./cartpage.component.css']
})
export class CartpageComponent implements OnInit {

  paymentform!:FormGroup;

  constructor
  (
    private fb:FormBuilder,
  ) { }

  ngOnInit(): void {
    this.paymentform = this.fb.group({
      purchaseamount:['',[Validators.required]],
      paymentmode:['',[Validators.required]],
      cardnumber:['',[Validators.required,Validators.minLength(12),Validators.maxLength(12)]],
      expirydate:['',[Validators.required]],
      cvv_number:['',[Validators.required,Validators.minLength(3),Validators.maxLength(3)]]
    })
  }

}
