import { Injectable } from '@angular/core';
import { Product } from '../models/product.model';
@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  productlist:any;

  
  products: Product[] = [
    {
      id:1,
      name:"mobile",
      price:100,
      description:"good product",
    },
    {
      id:2,
      name:"laptop",
      price:200,
      description:"good product",
    },
  ];
  constructor() { }
  onGetProduct(id:any){
    return this.products.find(x=>x.id == id);
    console.log("25",this.products.find(x=>x.id == id));
  }

  onGet(){
    return this.products;
    // return this.productlist;
  }
  onAdd(product: Product){
    this.products.push(product);
    localStorage.setItem("productlist",JSON.stringify(this.products));
    this.productlist = localStorage.getItem("productlist");
  }
  onDelete(product: Product){ 
    let index = this.products.findIndex(function(x){
      return x.id == product.id;
    });
    this.products.splice(index);
  }

  onUpdate(product : Product){
    let index = this.products.findIndex(function(x){
      return x.id == product.id
    });
    this.products[index] = product;
    localStorage.setItem("productlist",JSON.stringify(this.products));
    localStorage.getItem("productlist");
  }
}
