import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/product.model';
import { ProductsService } from 'src/app/services/products.service';
import { ProductsComponent } from '../products.component';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  id:any;
  productadd!: FormGroup;
  constructor
  (
    private fb:FormBuilder,
    private router:Router, 
    private route:ActivatedRoute,
    private productService:ProductsService
  ) { }

  ngOnInit(): void {
    this.productadd=this.fb.group({
      name:['',[Validators.required]],
      price:['',[Validators.required]],
      description:['',[Validators.required]],

    });
    // localStorage.getItem("product",Product);
    // localStorage.setItem("product","Product");
  }

  // onSubmit(form: NgForm){
  //   let product:Product = {
  //     id:this.productService.products.length+1,
  //     name:this.productadd.value.name,
  //     price:this.productadd.value.price,
  //     description:this.productadd.value.description
  //   }
  //   this.productService.onAdd(product);
  //   console.log(product)
  //   this.router.navigateByUrl('');
  // }

  saveproduct(){
    let product:Product = {
      id:this.productService.products.length+1,
      name:this.productadd.value.name,
      price:this.productadd.value.price,
      description:this.productadd.value.description
    }
    this.productService.onAdd(product);
    console.log(product)
    this.router.navigateByUrl('');
  }
  
}
