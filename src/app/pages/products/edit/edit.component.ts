import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/models/product.model';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
 id:any;
 header:any;

product:any;

  constructor
  (
   private router:Router, 
   private route:ActivatedRoute,
   private productService:ProductsService
  ) 
  { }

  ngOnInit(): void { 
    this.id = this.route.snapshot.paramMap.get('id'); 
    this.product = this.productService.onGetProduct(this.id);
  }

  onSubmit(form: NgForm){
    let product:Product = form.form.getRawValue();
    console.log("editproduct",product);
    
    this.productService.onUpdate(product);
    
    this.router.navigateByUrl('');
    console.log("edit form");
  }
  
}
