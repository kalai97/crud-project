import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product.model';
import { ProductsService } from 'src/app/services/products.service';
@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  // products: Product[];  
  products:any;
  productlist:any;
  constructor
  (
    private productService:ProductsService
  )
   {
    //  productlist:this.productService.productlist;
    }


  ngOnInit(): void {
    this.products = this.productService.onGet();
    // this.productlist = this.productService.onGet();

  }
 
  onDelete(product:Product){
    this.productService.onDelete(product);
  }


}
