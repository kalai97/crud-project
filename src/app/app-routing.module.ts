import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShoppingpageComponent } from './layout/shoppingpage/shoppingpage.component';
import { AddComponent } from './pages/products/add/add.component';
import { EditComponent } from './pages/products/edit/edit.component';
import { ProductsComponent } from './pages/products/products.component';
import { LoginpageComponent } from './layout/loginpage/loginpage.component'
import { RegisterpageComponent } from './layout/registerpage/registerpage.component';
import { CartpageComponent } from './layout/cartpage/cartpage.component';

const routes: Routes = [
 {
   path:"",
   component:ProductsComponent
 },
 {
   path:"product/add",
   component:AddComponent
 },
 {
   path:"product/edit/:id",
   component:EditComponent
 },
 {
   path:"shoppage",
   component:ShoppingpageComponent
 },
 {
   path:'signup',
   component:RegisterpageComponent
 },
 {
   path:'login',
   component:LoginpageComponent
 },
 {
   path:'paymentpage',
   component:CartpageComponent
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
